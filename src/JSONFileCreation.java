import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Stack;

public class JSONFileCreation {

    // Creating a file object which is written to the file and stores the program object
    JSONObject fileObject = new JSONObject();
    JSONArray programObject  = new JSONArray();

    // Stack of list of JSON objects to keep track pf the details of a current statement or expression
    Stack<ArrayList<JSONObject>> objectListStack = new Stack<>();

    // Stack of booleans to keep track where statements and expressions should be placed in the JSON file
    Stack<Boolean> indent = new Stack<>();

    //Stack to keep track if the true block has been already added to the json file or not
    Stack<Integer> ifBlockCounterStack = new Stack<>();

    // Method to add an empty list of JSON objects to the top of the stack
    public void pushObjectList() {
        ArrayList<JSONObject> list = new ArrayList<JSONObject>();
        this.objectListStack.push(list);
    }

    // Method to pop the last list from the stack
    public void popObjectList() {
        this.objectListStack.pop();
    }

}




