// Generated from smallLang.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link smallLangParser}.
 */
public interface smallLangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link smallLangParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(smallLangParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(smallLangParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(smallLangParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(smallLangParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDecl(smallLangParser.FunctionDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDecl(smallLangParser.FunctionDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#variableDecl}.
	 * @param ctx the parse tree
	 */
	void enterVariableDecl(smallLangParser.VariableDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#variableDecl}.
	 * @param ctx the parse tree
	 */
	void exitVariableDecl(smallLangParser.VariableDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(smallLangParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(smallLangParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#printStatement}.
	 * @param ctx the parse tree
	 */
	void enterPrintStatement(smallLangParser.PrintStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#printStatement}.
	 * @param ctx the parse tree
	 */
	void exitPrintStatement(smallLangParser.PrintStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(smallLangParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(smallLangParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(smallLangParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(smallLangParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(smallLangParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(smallLangParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#rtrnStatement}.
	 * @param ctx the parse tree
	 */
	void enterRtrnStatement(smallLangParser.RtrnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#rtrnStatement}.
	 * @param ctx the parse tree
	 */
	void exitRtrnStatement(smallLangParser.RtrnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#formalParam}.
	 * @param ctx the parse tree
	 */
	void enterFormalParam(smallLangParser.FormalParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#formalParam}.
	 * @param ctx the parse tree
	 */
	void exitFormalParam(smallLangParser.FormalParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#formalParams}.
	 * @param ctx the parse tree
	 */
	void enterFormalParams(smallLangParser.FormalParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#formalParams}.
	 * @param ctx the parse tree
	 */
	void exitFormalParams(smallLangParser.FormalParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(smallLangParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(smallLangParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(smallLangParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(smallLangParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpression(smallLangParser.SimpleExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpression(smallLangParser.SimpleExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(smallLangParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(smallLangParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(smallLangParser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(smallLangParser.FactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(smallLangParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(smallLangParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(smallLangParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(smallLangParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#actualParams}.
	 * @param ctx the parse tree
	 */
	void enterActualParams(smallLangParser.ActualParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#actualParams}.
	 * @param ctx the parse tree
	 */
	void exitActualParams(smallLangParser.ActualParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#subExpression}.
	 * @param ctx the parse tree
	 */
	void enterSubExpression(smallLangParser.SubExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#subExpression}.
	 * @param ctx the parse tree
	 */
	void exitSubExpression(smallLangParser.SubExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary(smallLangParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary(smallLangParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(smallLangParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(smallLangParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntegerLiteral(smallLangParser.IntegerLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntegerLiteral(smallLangParser.IntegerLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(smallLangParser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(smallLangParser.BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#floatLiteral}.
	 * @param ctx the parse tree
	 */
	void enterFloatLiteral(smallLangParser.FloatLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#floatLiteral}.
	 * @param ctx the parse tree
	 */
	void exitFloatLiteral(smallLangParser.FloatLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(smallLangParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(smallLangParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link smallLangParser#auto}.
	 * @param ctx the parse tree
	 */
	void enterAuto(smallLangParser.AutoContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallLangParser#auto}.
	 * @param ctx the parse tree
	 */
	void exitAuto(smallLangParser.AutoContext ctx);
}