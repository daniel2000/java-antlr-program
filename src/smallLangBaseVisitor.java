// Generated from smallLang.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.json.simple.JSONObject;

/**
 * This class provides an empty implementation of {@link smallLangVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class smallLangBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements smallLangVisitor<T> {


	// Parameter of class smallLangBaseVisitor to store object of type JSONFileCreation
	JSONFileCreation jsonFile;

	// Constructor
	public smallLangBaseVisitor(JSONFileCreation jsonFile) {
		this.jsonFile = jsonFile;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProgram(smallLangParser.ProgramContext ctx) {
		visitChildren(ctx);
		// Storing the pair with key "Program" and value the program object in the file object
		jsonFile.fileObject.put("Program", jsonFile.programObject);
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatement(smallLangParser.StatementContext ctx) {
		// Here nothing is done since no extra information needs to be added for a statement node
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunctionDecl(smallLangParser.FunctionDeclContext ctx) {
		// This indicates to it's children contexts that,
		// these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);

		jsonFile.indent.pop();

		JSONObject functionDeclarationObject = new JSONObject();

		// Adding all the attributes related to function declaration object
		functionDeclarationObject.put("FunctionDecl",jsonFile.objectListStack.peek());

		//If the current function decl context is not going to be placed inside another object,
		//The function declaration object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the function declaration object to the programObject
			jsonFile.programObject.add(functionDeclarationObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement,
			// this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(functionDeclarationObject);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableDecl(smallLangParser.VariableDeclContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject variableDeclarationObject = new JSONObject();

		// Adding all the attributes related to variable declaration object
		variableDeclarationObject.put("VariableDecl",jsonFile.objectListStack.peek());

		//If the variable function decl context is not going to be placed inside another object,
		//The variable declaration object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(variableDeclarationObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(variableDeclarationObject);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssignment(smallLangParser.AssignmentContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject assignmentObject = new JSONObject();

		// Adding all the attributes related to assignment object
		assignmentObject.put("Assignment",jsonFile.objectListStack.peek());

		//If the current assignment context is not going to be placed inside another object,
		//The assignment object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(assignmentObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(assignmentObject);
		}

		return null;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrintStatement(smallLangParser.PrintStatementContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject printObject = new JSONObject();

		// Adding all the attributes related to print object
		printObject.put("Print",jsonFile.objectListStack.peek());

		//If the current print context is not going to be placed inside another object,
		//The print object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(printObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(printObject);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIfStatement(smallLangParser.IfStatementContext ctx) {
		// Adding an item to the stack to indicate whether the true or false block is going to be written
		jsonFile.ifBlockCounterStack.push(0);

		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject ifObject = new JSONObject();

		// Adding all the attributes related to if object
		ifObject.put("If",jsonFile.objectListStack.peek());



		//If the current if context is not going to be placed inside another object,
		//The if object is put in the program object
		if(jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(ifObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(ifObject);
		}

		jsonFile.ifBlockCounterStack.pop();
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitForStatement(smallLangParser.ForStatementContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject forObject = new JSONObject();

		// Adding all the attributes related to for object
		forObject.put("For",jsonFile.objectListStack.peek());


		//If the current for context is not going to be placed inside another object,
		//The for object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(forObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(forObject);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhileStatement(smallLangParser.WhileStatementContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject whileObject = new JSONObject();

		// Adding all the attributes related to while object
		whileObject.put("While",jsonFile.objectListStack.peek());


		//If the current while context is not going to be placed inside another object,
		//The while object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(whileObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(whileObject);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRtrnStatement(smallLangParser.RtrnStatementContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject returnObject = new JSONObject();

		// Adding all the attributes related to Return object
		returnObject.put("Return",jsonFile.objectListStack.peek());


		//If the current return context is not going to be placed inside another object,
		//The return object is put in the program object
		if(jsonFile.indent.empty()){
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(returnObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(returnObject);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFormalParam(smallLangParser.FormalParamContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject formalParamObject = new JSONObject();

		// Adding all the attributes related to FormalParam object
		formalParamObject.put("FormalParam",jsonFile.objectListStack.peek());

		//If the current formalParam context is not going to be placed inside another object,
		//The formalParam object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(formalParamObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(formalParamObject);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFormalParams(smallLangParser.FormalParamsContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject formalParamsObject = new JSONObject();

		// Adding all the attributes related to FormalParams object
		formalParamsObject.put("FormalParams",jsonFile.objectListStack.peek());


		//If the current formalParams context is not going to be placed inside another object,
		//The formalParams object is put in the program object
		if(jsonFile.indent.empty()){
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(formalParamsObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(formalParamsObject);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBlock(smallLangParser.BlockContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		// Adding a new list of json objects on the stack
		jsonFile.pushObjectList();
		// Visiting the children
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject blockObject = new JSONObject();


		// For other block ctx calls
		// Adding all the attributes related to Block object
		blockObject.put("Block",jsonFile.objectListStack.peek());


		//If the current block context is not going to be placed inside another object,
		//The formalParams object is put in the program object
		if(jsonFile.indent.empty()){
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(blockObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(blockObject);
		}


		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression(smallLangParser.ExpressionContext ctx) {
		// Checking if the term has relational operators or not
		if (ctx.children.size() == 1) {
			return visitChildren(ctx);
		} else {
			// Adding a new list of json objects on the stack
			jsonFile.pushObjectList();
			visitChildren(ctx);

			String relOp = "";
			// Obtaining all the additive operators
			for(int i = 0;i<ctx.children.size();i++) {
				if (i % 2 == 1) {
					relOp += ctx.children.get(i);
					if (i+2 != ctx.children.size()) {
						relOp +=",";
					}
				}
			}

			JSONObject temp = new JSONObject();
			temp.put("RelationalOp",relOp);

			// Adding the relational operator pair
			jsonFile.objectListStack.peek().add(temp);

			JSONObject expressionObject = new JSONObject();
			// Assigning the details related to the expression to the key Expression
			expressionObject.put("Expression",jsonFile.objectListStack.peek());

			// Removing the statement object containing the information about the expression which is now stored  in the above expression object
			jsonFile.popObjectList();

			// Adding the expression object ot the top list of statement objects
			jsonFile.objectListStack.peek().add(expressionObject);

		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSimpleExpression(smallLangParser.SimpleExpressionContext ctx) {
		// Checking if the term has additive operators or not
		if (ctx.children.size() == 1) {
			return visitChildren(ctx);
		} else {

			jsonFile.pushObjectList();
			visitChildren(ctx);

			String addOp = "";
			// Obtaining all the additive operators
			for(int i = 0;i<ctx.children.size();i++) {
				if (i % 2 == 1) {
					addOp += ctx.children.get(i);

					if (i+2 != ctx.children.size()) {
						addOp +=",";
					}
				}
			}

			JSONObject temp = new JSONObject();
			temp.put("AdditiveOp",addOp);

			// Adding the additive operator pair
			jsonFile.objectListStack.peek().add(temp);

			JSONObject simpleExpressionObject = new JSONObject();
			// Assigning the details related to the simple expression to the key SimpleExpression
			simpleExpressionObject.put("SimpleExpression",jsonFile.objectListStack.peek());

			// Removing the statement object containing the information about the simple expression which is now stored in the above simple expression object
			jsonFile.popObjectList();

			// Adding the simple expression object ot the top list of statement objects
			jsonFile.objectListStack.peek().add(simpleExpressionObject);

		}
		return null;

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTerm(smallLangParser.TermContext ctx) {
		// Checking if the term has multiplicative operators or not
		if (ctx.children.size() == 1) {
			return visitChildren(ctx);
		} else {

			jsonFile.pushObjectList();
			visitChildren(ctx);

			String multOp = "";
			// Obtaining all the multiplicative operators
			for(int i = 0;i<ctx.children.size();i++) {
				if (i % 2 == 1) {
					multOp += ctx.children.get(i);

					if (i+2 != ctx.children.size()) {
						multOp +=",";
					}
				}


			}

			JSONObject temp = new JSONObject();
			temp.put("MultiplicativeOp",multOp);

			// Adding the multiplicative operator pair
			jsonFile.objectListStack.peek().add(temp);

			JSONObject termObject = new JSONObject();
			// Assigning the details related to the term to the key Term
			termObject.put("Term",jsonFile.objectListStack.peek());

			// Removing the statement object containing the information about the term with is stored in the above term object
			jsonFile.popObjectList();

			// Adding the term object ot the top list of statement objects
			jsonFile.objectListStack.peek().add(termObject);

		}
		return null;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFactor(smallLangParser.FactorContext ctx) {
		// Here nothing is done since no extra information needs to be added for a factor node
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLiteral(smallLangParser.LiteralContext ctx) {
		// Here nothing is done since no extra information needs to be added for a literal node
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunctionCall(smallLangParser.FunctionCallContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		jsonFile.pushObjectList();
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject functionCallObject = new JSONObject();
		functionCallObject.put("FunctionCall",jsonFile.objectListStack.peek());


		//If the current function call  context is not going to be placed inside another object,
		//The function call object is put in the program object
		if (jsonFile.indent.empty()) {
			// Adding the assignment object to the programObject
			jsonFile.programObject.add(functionCallObject);
			// Removing the last list of json objects
			jsonFile.popObjectList();
		} else if(jsonFile.indent.peek()){
			// If this statement is going to be put inside another statement, this statement's object needs to be added to the
			// object list of the last stack
			jsonFile.popObjectList();
			jsonFile.objectListStack.peek().add(functionCallObject);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitActualParams(smallLangParser.ActualParamsContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.indent.push(true);
		jsonFile.pushObjectList();
		visitChildren(ctx);
		jsonFile.indent.pop();

		JSONObject acutalParamsObject = new JSONObject();
		acutalParamsObject.put("ActualParams",jsonFile.objectListStack.peek());

		// Removing the statement object containing the information about the acutal params which is now stored in the above actual params object
		jsonFile.popObjectList();

		// Adding the actual params object at the top list of statement objects
		jsonFile.objectListStack.peek().add(acutalParamsObject);
		return null;

	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSubExpression(smallLangParser.SubExpressionContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.pushObjectList();
		visitChildren(ctx);


		JSONObject subExpressionObject = new JSONObject();
		subExpressionObject.put("SubExpression",jsonFile.objectListStack.peek());

		// Removing the statement object containing the information about the sub expression which is now stored in the above sub expression object
		jsonFile.popObjectList();

		// Adding the sub expression object at the top list of statement objects
		jsonFile.objectListStack.peek().add(subExpressionObject);
		return null;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnary(smallLangParser.UnaryContext ctx) {
		// This indicates to it's children contexts that, these child objects are going to be placed inside another object
		// and not put directly on the programObject
		jsonFile.pushObjectList();
		visitChildren(ctx);


		JSONObject temp = new JSONObject();
		temp.put("OppositeOperator",""+ctx.children.get(0)+"");

		jsonFile.objectListStack.peek().add(temp);

		JSONObject unaryObject = new JSONObject();
		unaryObject.put("Unary",jsonFile.objectListStack.peek());

		// Removing the statement object containing the information about the unary which is now stored in the above unary object
		jsonFile.popObjectList();

		// Adding the unary object ot the top list of statement objects
		jsonFile.objectListStack.peek().add(unaryObject);
		return null;

	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIdentifier(smallLangParser.IdentifierContext ctx) {
		// Adding the id of an identifier and adding it to the object list
		JSONObject identifierObject = new JSONObject();
		identifierObject.put("Identifier",""+ctx.children.get(0)+"");
		jsonFile.objectListStack.peek().add(identifierObject);
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIntegerLiteral(smallLangParser.IntegerLiteralContext ctx) {
		JSONObject literalObject = new JSONObject();
		// Adding all the digits to a temp string
		String temp = "";
		for (int i = 0;i<ctx.children.size();i++) {
			temp += ctx.children.get(i);
		}

		// Adding the key value pair with the integer value to the current object list
		literalObject.put("IntegerLiteral", temp);
		jsonFile.objectListStack.peek().add(literalObject);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBooleanLiteral(smallLangParser.BooleanLiteralContext ctx) {
		JSONObject literalObject = new JSONObject();
		// Adding the key value pair with the boolean value to the current object list
		literalObject.put("BooleanLiteral", ""+ctx.children.get(0)+"");
		jsonFile.objectListStack.peek().add(literalObject);
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFloatLiteral(smallLangParser.FloatLiteralContext ctx) {
		JSONObject literalObject = new JSONObject();

		// Adding all the digits to a temp string
		String temp = "";
		for (int i = 0;i<ctx.children.size();i++) {
			temp += ctx.children.get(i);
		}

		// Adding the key value pair with the float value to the current object list
		literalObject.put("FloatLiteral", temp);
		jsonFile.objectListStack.peek().add(literalObject);
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType(smallLangParser.TypeContext ctx) {
		JSONObject typeObject = new JSONObject();
		// Adding the key value pair with the type to the current object list
		typeObject.put("Type", ""+ctx.children.get(0)+"");
		jsonFile.objectListStack.peek().add(typeObject);
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAuto(smallLangParser.AutoContext ctx) {
		JSONObject autoObject = new JSONObject();
		// Adding the key value pair with the type to the current object list
		autoObject.put("Type",""+ ctx.children.get(0)+"");
		jsonFile.objectListStack.peek().add(autoObject);
		return visitChildren(ctx);
	}

}