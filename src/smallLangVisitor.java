// Generated from smallLang.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link smallLangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface smallLangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link smallLangParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(smallLangParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(smallLangParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#functionDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDecl(smallLangParser.FunctionDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#variableDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDecl(smallLangParser.VariableDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(smallLangParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#printStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintStatement(smallLangParser.PrintStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(smallLangParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#forStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(smallLangParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(smallLangParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#rtrnStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRtrnStatement(smallLangParser.RtrnStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#formalParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParam(smallLangParser.FormalParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#formalParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParams(smallLangParser.FormalParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(smallLangParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(smallLangParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpression(smallLangParser.SimpleExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(smallLangParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactor(smallLangParser.FactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(smallLangParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(smallLangParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#actualParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActualParams(smallLangParser.ActualParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#subExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubExpression(smallLangParser.SubExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(smallLangParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(smallLangParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#integerLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerLiteral(smallLangParser.IntegerLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#booleanLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanLiteral(smallLangParser.BooleanLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#floatLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatLiteral(smallLangParser.FloatLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(smallLangParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link smallLangParser#auto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAuto(smallLangParser.AutoContext ctx);
}