grammar smallLang;

/*
PARSER RULES
*/

program : (statement)*;

/* Statement Rules */
statement : (variableDecl ';') | (assignment ';') | (printStatement ';') | ifStatement | forStatement | whileStatement | (rtrnStatement ';') | functionDecl | block ;

functionDecl: 'ff' identifier '(' (formalParams)? ')' ':' (type|auto) block;

variableDecl : 'let' identifier ':' (type | auto) '=' expression;

assignment : identifier '=' expression;

printStatement : 'print' expression;

ifStatement : 'if' '(' expression ')' block ('else' block)?;

forStatement : 'for' '(' (variableDecl)? ';' expression ';' (assignment)? ')' block;

whileStatement: 'while' '(' expression ')' block;

rtrnStatement : 'return' expression;

formalParam : identifier ':' type;

formalParams : formalParam (',' formalParam)*;



block: '{' (statement)* '}';

/* Expression Rules */
expression : simpleExpression (RELATIONALOP simpleExpression)*;

simpleExpression : term ((ADDITIVEOP|MINUS|OR) term)*;

term : factor ((MULTIPLICATIVEOP|AND) factor)*;

factor : literal | identifier | functionCall | subExpression | unary;

literal : booleanLiteral | integerLiteral | floatLiteral;

functionCall : identifier '(' (actualParams)? ')';

actualParams : expression (',' expression)*;

subExpression : '(' expression ')';

unary : (MINUS | 'not') expression;






identifier : ('_' | LETTER) ('_' | LETTER | DIGIT)*;

integerLiteral : DIGIT (DIGIT)*;
booleanLiteral : 'true' |'false';
floatLiteral: DIGIT (DIGIT)* '.' DIGIT (DIGIT)*;
type : 'float' | 'int' | 'bool';
auto : 'auto';

/*
Lexer Rules
*/
MINUS: '-';
OR : 'or';
AND: 'and';


LETTER: [A-Za-z]+;
DIGIT : [0-9];



MULTIPLICATIVEOP : '*' | '/' | AND;

ADDITIVEOP : ('+' | MINUS | OR);

RELATIONALOP : '<' | '>' | '==' | '<>' | '<=' | '>=';

WHITESPACE : ' ' -> skip;
NEWLINE : '\r\n'  -> skip ;
LINECOMMENT :  '//' ~[\r\n]* -> skip;
COMMENT: '/*' .*? '*/' -> skip;



