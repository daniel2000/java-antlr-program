ff output(x:bool): bool {
    // visual break
    let visualBreak:int = 0000000000;

    let y:int = 3;
    let temp:int = y;

    // For loop without declaration or assignment
    for(;y>=0;) {
        print y;
        y = y-1;
    }

    print visualBreak;

    // While loop
    while(temp>=0) {
        print temp;
        temp = temp - 1 ;
    }

    print visualBreak;

    // Testing for multiple simple expression in an expresion
    if(5+5/5 <= 6*2) {
        print 1;
    }

    print visualBreak;

    // Testing for and multiplicative op
    if(x and true) {
        print 100;
    }

    print visualBreak;

    // Testing for or additive op
    if(x or false) {
        print 100;
    }

    print visualBreak;
    let z:bool = false;
    // Testing unary both not and -
    if(not z) {
        print 5-2;
    }

    print visualBreak;

    //Testing sub expression
    let test:int = 12;
    while ( (5+10) >  test) {
        print test;
        test =test+1;
    }

return true;
}

print output(true);